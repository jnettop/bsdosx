/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/client/JnettopSession.java,v 1.4 2006-04-30 12:52:49 merunka Exp $
 *
 */

package info.kubs.jnettop.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JnettopSession {
	private BufferedReader  inputReader;
	private OutputStreamWriter	outputWriter;
	private Pattern responsePattern;
	
	private String responseCommandName;
	private String responseStatus;
	private String responseData;
	
	private final int STATE_CREATED		= 0;
	private final int STATE_COMMAND		= 1;
	private final int STATE_RUNNING		= 2;
	
	private int state;
	
	private Thread runningThread;
	private HashMap<Long, JnettopStream> streams;
	private HashMap<String, String> resolver;
	private JnettopStatistics statistics;
	
	private JnettopInterface[] interfaces;
	
	private JnettopSessionListener listener;
	private JnettopSessionSite site;
	
	public JnettopSession(InputStream inputStream, OutputStream outputStream) throws JnettopSessionException {
		Charset asciiCharset = Charset.forName("ASCII");
		if (asciiCharset == null) {
			throw new JnettopSessionException("ASCII Charset is not supported in underlaying JDK.");
		}
		this.inputReader = new BufferedReader(new InputStreamReader(inputStream, asciiCharset));
		this.outputWriter = new OutputStreamWriter(outputStream, asciiCharset);
		this.responsePattern = Pattern.compile("^([^:]+):([^ ]+) (.+)$");
		state = STATE_CREATED;
		streams = new HashMap<Long, JnettopStream>();
		resolver = new HashMap<String, String>();
		statistics = new JnettopStatistics();
	}
	
	private void sendLine(String text) throws IOException {
		outputWriter.write(text);
		outputWriter.flush();
	}
	
	private void parseResponse() throws IOException, JnettopSessionException {
		String line = inputReader.readLine();
		if (line == null) {
			throw new JnettopSessionException("Remote host unexpectedly closed connection.");
		}
		Matcher m = responsePattern.matcher(line);
		if (!m.matches()) {
			throw new JnettopSessionException("Unexpected answer.");
		}
		responseCommandName = m.group(1);
		responseStatus = m.group(2);
		responseData = m.group(3);
	}
	
	public void initialize() throws IOException, JnettopSessionException {
		if (state != STATE_CREATED) {
			throw new IllegalStateException("You can call initialize() only after object construction.");
		}
		sendLine("HELLO 1\n");
		parseResponse();
		if (!"HELLO".equals(responseCommandName) || !"OK".equals(responseStatus)) {
			throw new JnettopSessionException("Unexpected answer to HELLO message");
		}
		sendLine("INTERFACES\n");
		parseResponse();
		if (!"INTERFACES".equals(responseCommandName) || !"OK".equals(responseStatus)) {
			throw new JnettopSessionException("Unexpected answer to INTERFACES message");
		}
		
		ArrayList<JnettopInterface> interfaceList = new ArrayList<JnettopInterface>();
		while (true) {
			parseResponse();
			if (!"INTERFACES".equals(responseCommandName)) {
				throw new JnettopSessionException("Unexpected answer enumerating interfaces");
			}
			if ("END".equals(responseStatus)) {
				break;
			}
			if (!"INFO".equals(responseStatus)) {
				throw new JnettopSessionException("Unexpected answer enumerating interfaces");
			}
			
			String data[] = responseData.split("\t");
			if (data.length != 3) {
				throw new JnettopSessionException("Unexpected answer enumerating interfaces");				
			}
			try {
				interfaceList.add(new JnettopInterface(data[0], Integer.parseInt(data[1]), data[2]));
			} catch (NumberFormatException e) {
				throw new JnettopSessionException("Unexpected answer enumerating interfaces");
			}
		}
		this.interfaces = new JnettopInterface[interfaceList.size()];
		this.interfaces = interfaceList.toArray(this.interfaces); 

		state = STATE_COMMAND;
	}
	
	public JnettopInterface[] getInterfaces() {
		return interfaces;
	}
	
	public void setSessionListener(JnettopSessionListener listener) {
		this.listener = listener;
	}
	
	public void setSessionSite(JnettopSessionSite site) {
		this.site = site;
	}
	
	public void run() throws JnettopSessionException, IOException {
		if (state != STATE_COMMAND) {
			throw new IllegalStateException("You can call run() only after successfull initialization.");
		}
		
		streams.clear();
		resolver.clear();
		
		sendLine("RUN\n");
		parseResponse();
		if (!"RUN".equals(responseCommandName) || !"OK".equals(responseStatus)) {
			throw new JnettopSessionException("Unexpected answer trying to execute listening");
		}
		
		state = STATE_RUNNING;
		
		runningThread = new Thread(new Runnable() {
			public void run() {
				try {
					runInternal();
				} catch (Exception e) {
					if (site != null) {
						StringWriter sw = new StringWriter();
						e.printStackTrace(new PrintWriter(sw));
						site.showMessage("Error while running background processing: " + sw.toString());
					}
				}
			}
		});
		runningThread.start();
	}
	
	private void runInternal() throws IOException, JnettopSessionException {		
		while (true) {
			parseResponse();
			if ("STOP".equals(responseCommandName) && "OK".equals(responseStatus)) {
				state = STATE_COMMAND;
				return;
			}
			if (!"SOOB".equals(responseCommandName)) {
				continue;
			}
			switch (responseStatus.charAt(0)) {
			case 'S':
				{
					String data[] = responseData.split("\t");
					if (data.length != 12) {
						continue;
					}
					parseStatistics(data);
				} break;
			case 'N':
				{
					String data[] = responseData.split("\t");
					if (data.length != 6) {
						continue;
					}
					parseNewStream(data);
				} break;
			case 'U':
				{
					String data[] = responseData.split("\t", 14);
					if (data.length != 14) {
						continue;
					}
					parseUpdateStream(data);
				} break;
			case 'D':
				{
					parseDeleteStream(responseData);
				} break;
			case 'L':
				{
					String data[] = responseData.split("\t");
					if (data.length != 2) {
						continue;
					}
					parseLookup(data);
				} break;
			case 'E':
				{
					if (this.listener != null) {
						this.listener.dataAvailable(this);
					}
				} break;
			}
		}
	}
	
	private void parseStatistics(String data[]) {
		statistics.totalLocalBytes = Long.parseLong(data[0]);
		statistics.totalRemoteBytes = Long.parseLong(data[1]);
		statistics.totalBytes = Long.parseLong(data[2]);
		statistics.totalLocalPackets = Long.parseLong(data[3]);
		statistics.totalRemotePackets = Long.parseLong(data[4]);
		statistics.totalPackets = Long.parseLong(data[5]);
		statistics.totalLocalBps = Long.parseLong(data[6]);
		statistics.totalRemoteBps = Long.parseLong(data[7]);
		statistics.totalBps = Long.parseLong(data[8]);
		statistics.totalLocalPps = Long.parseLong(data[9]);
		statistics.totalRemotePps = Long.parseLong(data[10]);
		statistics.totalPps = Long.parseLong(data[11]);
	
	}
	
	private void parseNewStream(String[] data) {
		JnettopStream stream = new JnettopStream(this);
		stream.uid = Long.parseLong(data[0], 16);
		stream.localAddress = data[1];
		stream.remoteAddress = data[2];
		stream.protocol = data[3];
		stream.localPort = Integer.parseInt(data[4]);
		stream.remotePort = Integer.parseInt(data[5]);
		synchronized (streams) {
			streams.put(stream.uid, stream);
		}
	}
	
	private void parseUpdateStream(String[] data) {
		JnettopStream stream;
		synchronized (streams) {
			stream = streams.get(Long.parseLong(data[0], 16));
		}
		stream.localBytes = Long.parseLong(data[1]);
		stream.remoteBytes = Long.parseLong(data[2]);
		stream.totalBytes = Long.parseLong(data[3]);
		stream.localPackets = Long.parseLong(data[4]);
		stream.remotePackets = Long.parseLong(data[5]);
		stream.totalPackets = Long.parseLong(data[6]);
		stream.localBps = Long.parseLong(data[7]);
		stream.remoteBps = Long.parseLong(data[8]);
		stream.totalBps = Long.parseLong(data[9]);
		stream.localPps = Long.parseLong(data[10]);
		stream.remotePps = Long.parseLong(data[11]);
		stream.totalPps = Long.parseLong(data[12]);
		if (data[13].length() > 0)
			stream.filterData = data[13];
	}
	
	private void parseDeleteStream(String data) {
		synchronized (streams) {
			streams.remove(Long.parseLong(data, 16));
		}
	}
	
	private void parseLookup(String data[]) {
		synchronized (resolver) {
			resolver.put(data[0], data[1]);
		}
	}
	
	public void stop() throws JnettopSessionException, IOException {
		if (state != STATE_RUNNING) {
			throw new IllegalStateException("You can call stop() only when the capture is running.");
		}
		sendLine("STOP\n");
		while (true) {
			try {
				runningThread.join();
				return;
			} catch (InterruptedException e) {
			}
		}
	}
	
	public void exit() throws IOException, JnettopSessionException
	{
		if (state == STATE_RUNNING) {
			throw new IllegalStateException("Please issue stop() before exit()");
		}
		sendLine("EXIT\n");
		parseResponse();		
	}
	
	public void close() {
		try {
			if (state == STATE_RUNNING) {
				stop();
			}
			exit();
		} catch (Exception e) {
			
		}
		
		try {
			this.inputReader.close();
			this.outputWriter.close();
		} catch (Exception e) {
			
		}
	}
	
	public HashMap<Long, JnettopStream> getStreams() {
		return streams;
	}
	
	public JnettopStatistics getStatistics() {
		return statistics;
	}
	
	public String resolveAddress(String address) {
		synchronized (resolver) {
			return resolver.get(address);
		}
	}
	
	public void selectInterface(String interfaceName) throws IOException, JnettopSessionException {
		if (state != STATE_COMMAND) {
			throw new IllegalStateException("You can call selectInterface() only after successfull initialization.");
		}
		sendLine("INTERFACE \"" + interfaceName + "\"\n");
		parseResponse();
		if (!"INTERFACE".equals(responseCommandName) || !"OK".equals(responseStatus)) {
			throw new JnettopSessionException("Could not select interface");
		}
	}
}
