/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/JnettopConnectionData.java,v 1.2 2006-04-30 12:52:48 merunka Exp $
 *
 */

package info.kubs.jnettop.gui;

import java.util.prefs.Preferences;

public class JnettopConnectionData implements Cloneable {
	private String displayName;
	private String columnsConfiguration;
	private JnettopConnectionFactory factory;
	
	public JnettopConnectionData(JnettopConnectionFactory factory, String prefix, Preferences prefs) {
		this(factory, prefs.get(prefix + ".displayName", "Unknown connection"), prefs.get(prefix + ".columnsConfiguration", ""));
	}
	
	public JnettopConnectionData(JnettopConnectionFactory factory, String displayName, String columnsConfiguration) {
		this.factory = factory;
		this.displayName = displayName;
		this.columnsConfiguration = columnsConfiguration;
	}
	
	public JnettopConnectionData(JnettopConnectionFactory factory, String displayName) {
		this(factory, displayName, "");
	}

	public String getDisplayName() {
		return displayName;
	}
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getColumnsConfiguration() {
		return columnsConfiguration;
	}

	public void setColumnsConfiguration(String columnsConfiguration) {
		this.columnsConfiguration = columnsConfiguration;
	}

	public JnettopConnectionFactory getFactory() {
		return factory;
	}
	
	public void store(String prefix, Preferences prefs) {
		prefs.put(prefix + ".displayName", displayName);
		prefs.put(prefix + ".columnsConfiguration", columnsConfiguration);
	}

	@Override
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
		}
		return null;
	}
	
	
}
