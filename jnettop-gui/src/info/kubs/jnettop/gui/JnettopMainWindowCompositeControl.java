/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/JnettopMainWindowCompositeControl.java,v 1.1 2006-04-30 12:52:48 merunka Exp $
 *
 */

package info.kubs.jnettop.gui;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabFolder2Adapter;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;

public class JnettopMainWindowCompositeControl extends Composite {
	private CTabFolder	tabFolder;
	
	private Link		welcomeLink;

	public JnettopMainWindowCompositeControl(Composite parent, int style) {
		super(parent, style);
		createControls();
	}
	
	private void createControls() {
		GridLayout gridLayout = new GridLayout();
		this.setLayout(gridLayout);
		
		GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true);
		tabFolder = new CTabFolder(this, SWT.CLOSE | SWT.FLAT | SWT.BORDER);
		tabFolder.addCTabFolder2Listener(new CTabFolder2Adapter() {
			@Override
			public void close(CTabFolderEvent event) {
				getDisplay().asyncExec(new Runnable() {
					public void run() {
						ensureCorrectWidgetIsShown();
					}
				});
			}
		});
		tabFolder.setLayoutData(gd);

		gd = new GridData(SWT.CENTER, SWT.CENTER, true, true);
		welcomeLink = new Link(this, 0);
		welcomeLink.setText("Welcome. First, <a href=\"create\">create</a> or <a href=\"open\">open</a> a connection.");
		welcomeLink.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.text.equals("create")) {
					JnettopGui.getInstance().invokeNewConnectionDialog();
					return;
				}
				if (e.text.equals("open")) {
					JnettopGui.getInstance().invokeOpenConnectionDialog();
					return;
				}
			}			
		});
		welcomeLink.setLayoutData(gd);
		
		ensureCorrectWidgetIsShown();
	}
	
	private void ensureCorrectWidgetIsShown() {
		boolean isThereATab = tabFolder.getItemCount() > 0; 
		tabFolder.setVisible(isThereATab);
		welcomeLink.setVisible(!isThereATab);
		((GridData)welcomeLink.getLayoutData()).exclude = isThereATab;
		((GridData)tabFolder.getLayoutData()).exclude = !isThereATab;
		layout();
	}

	public void addConnectionTab(JnettopConnectionData connectionData, JnettopConnection openedConnection) {
		final JnettopConnectionWidget wgt = new JnettopConnectionWidget(tabFolder, 0);
		wgt.setConnectionAndData(connectionData, openedConnection);
		
		CTabItem tabItem = new CTabItem(tabFolder, 0);
		tabItem.setControl(wgt);
		tabItem.setText(connectionData.getDisplayName());
		ensureCorrectWidgetIsShown();
		layout();
		
		tabItem.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				wgt.dispose();
			}
		});
		
		tabFolder.setSelection(tabItem);
	}
	
	public JnettopConnectionData getSelectedConnectionData() {
		int idx = tabFolder.getSelectionIndex();
		if (idx == -1)
			return null;
		
		CTabItem tabItem = tabFolder.getItem(idx);
		JnettopConnectionWidget wgt = (JnettopConnectionWidget) tabItem.getControl();
		return (JnettopConnectionData) wgt.getConnectionData();
	}
}