/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/Util.java,v 1.2 2006-04-30 12:52:48 merunka Exp $
 *
 */

package info.kubs.jnettop.gui;

public class Util {
	private static final char[] b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
			.toCharArray();
	
	static boolean isArrayEqual(byte[] a1, byte[] a2) {
		if (a1 == null && a2 == null)
			return true;
		if (a1 == null || a2 == null)
			return false;
		if (a1.length != a2.length)
			return false;
		for (int i=0; i<a1.length; i++) {
			if (a1[i] != a2[i])
				return false;
		}
		
		return true;
	}

	private static byte val(char foo) {
		if (foo == '=')
			return 0;
		for (int j = 0; j < b64.length; j++) {
			if (foo == b64[j])
				return (byte) j;
		}
		return 0;
	}

	public static byte[] fromBase64(String str) {
		char[] cha = str.toCharArray();
		byte[] foo = new byte[str.length()];
		int j = 0;
		for (int i = 0; i < str.length(); i += 4) {
			foo[j] = (byte) ((val(cha[i]) << 2) | ((val(cha[i + 1]) & 0x30) >>> 4));
			if (cha[i + 2] == (byte) '=') {
				j++;
				break;
			}
			foo[j + 1] = (byte) (((val(cha[i + 1]) & 0x0f) << 4) | ((val(cha[i + 2]) & 0x3c) >>> 2));
			if (cha[i + 3] == (byte) '=') {
				j += 2;
				break;
			}
			foo[j + 2] = (byte) (((val(cha[i + 2]) & 0x03) << 6) | (val(cha[i + 3]) & 0x3f));
			j += 3;
		}
		byte[] bar = new byte[j];
		System.arraycopy(foo, 0, bar, 0, j);
		return bar;
	}

	public static String toBase64(byte[] buf, int start, int length) {

		char[] tmp = new char[length * 2];
		int i, j, k;

		int foo = (length / 3) * 3 + start;
		i = 0;
		for (j = start; j < foo; j += 3) {
			k = (buf[j] >>> 2) & 0x3f;
			tmp[i++] = b64[k];
			k = (buf[j] & 0x03) << 4 | (buf[j + 1] >>> 4) & 0x0f;
			tmp[i++] = b64[k];
			k = (buf[j + 1] & 0x0f) << 2 | (buf[j + 2] >>> 6) & 0x03;
			tmp[i++] = b64[k];
			k = buf[j + 2] & 0x3f;
			tmp[i++] = b64[k];
		}

		foo = (start + length) - foo;
		if (foo == 1) {
			k = (buf[j] >>> 2) & 0x3f;
			tmp[i++] = b64[k];
			k = ((buf[j] & 0x03) << 4) & 0x3f;
			tmp[i++] = b64[k];
			tmp[i++] = (byte) '=';
			tmp[i++] = (byte) '=';
		} else if (foo == 2) {
			k = (buf[j] >>> 2) & 0x3f;
			tmp[i++] = b64[k];
			k = (buf[j] & 0x03) << 4 | (buf[j + 1] >>> 4) & 0x0f;
			tmp[i++] = b64[k];
			k = ((buf[j + 1] & 0x0f) << 2) & 0x3f;
			tmp[i++] = b64[k];
			tmp[i++] = (byte) '=';
		}
		return new String(tmp, 0, i);
	}
}
