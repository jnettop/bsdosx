package info.kubs.jnettop.gui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;

import com.swtdesigner.SWTResourceManager;

public class AboutDialog extends Dialog {

	public AboutDialog(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
		CLabel lbl = new CLabel(container, 0);
		lbl.setImage(SWTResourceManager.getImage(JnettopGui.class, "jnettop.png"));
		lbl.setText(String.format("Jnettop Gui version %1$s", JnettopGui.VERSION_NUMBER));
		lbl.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, true));
		
		Link lnk = new Link(container, SWT.MULTI);
		lnk.setText("This program is free software; you can redistribute it and/or modify\n" +
    "it under the terms of the GNU General Public License as published by\n" +
    "the Free Software Foundation; either version 2 of the License, or\n" +
    "(at your option) any later version.\n" +
    "\n" +
    "This program is distributed in the hope that it will be useful,\n" +
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
    "GNU General Public License for more details.\n" +
    "\n" +
    "You should have received a copy of the GNU General Public License\n" +
    "along with this program; if not, write to the Free Software\n" +
    "Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA\n" +
    "\n" +
    "This is Jnettop-Gui, a GUI for jnettop network traffic analyzer\n" +
    "Copyright (c) 2006 Jakub Skopal\n" +
    "Homepage: <a>http://jnettop.kubs.info</a>\n" +
    "\n" +
    "This program uses JSch, Java Secure Channel library\n" +
    "Copyright (c) 2002,2003,2004,2005,2006 Atsuhiko Yamanaka, JCraft,Inc.\n" +
    "Homepage: <a>http://www.jcraft.com/jsch/</a>\n" +
    "\n" +
    "This program uses SWT and Eclipse SDK\n" +
    "Copyright (c) The Eclipse Foundation\n" +
    "Homepage: <a>http://www.eclipse.org/platform/</a>"
    );
		lnk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Program.launch(e.text);
			}
		});
		
		return container;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("About");
	}
}
