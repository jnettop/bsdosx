/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/ssh/JnettopSshConnectionData.java,v 1.1 2006-04-30 12:52:25 merunka Exp $
 *
 */

package info.kubs.jnettop.gui.ssh;

import info.kubs.jnettop.gui.JnettopConnectionData;
import info.kubs.jnettop.gui.JnettopConnectionFactory;

import java.util.prefs.Preferences;

public class JnettopSshConnectionData extends JnettopConnectionData {

	private String	hostName;
	private int		port;
	private String	userName;
	private String	command;
	private String	identity;

	public JnettopSshConnectionData(JnettopConnectionFactory factory, String prefix, Preferences prefs) {
		super(factory, prefix, prefs);
		this.hostName = prefs.get(prefix + ".hostName", "localhost");
		this.port = Integer.parseInt(prefs.get(prefix + ".port", "22"));
		this.userName = prefs.get(prefix + ".userName", "root");
		this.command = prefs.get(prefix + ".command", "sudo /usr/bin/jnettop --display jnet");
		this.identity = prefs.get(prefix + ".identity", "");
	}

	public JnettopSshConnectionData(JnettopConnectionFactory factory, String hostName, int port, String userName, String command, String identity) {
		super(factory, userName + "@" + hostName + " via SSH");
		this.hostName = hostName;
		this.port = port;
		this.userName = userName;
		this.command = command;
		this.identity = identity;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Override
	public void store(String prefix, Preferences prefs) {
		super.store(prefix, prefs);
		prefs.put(prefix + ".hostName", hostName);
		prefs.put(prefix + ".port", Integer.toString(port));
		prefs.put(prefix + ".userName", userName);
		prefs.put(prefix + ".command", command);
		prefs.put(prefix + ".identity", identity);
	}	
}
