/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/ssh/JnettopSshConnection.java,v 1.1 2006-04-30 12:52:25 merunka Exp $
 *
 */

package info.kubs.jnettop.gui.ssh;

import info.kubs.jnettop.client.JnettopSession;
import info.kubs.jnettop.client.JnettopSessionSite;
import info.kubs.jnettop.gui.JnettopConnection;
import info.kubs.jnettop.gui.JnettopConnectionData;
import info.kubs.jnettop.gui.JnettopConnectionFactory;
import info.kubs.jnettop.gui.JnettopGui;
import info.kubs.jnettop.gui.PasswordMessageDialog;
import info.kubs.jnettop.gui.Util;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.HostKey;
import com.jcraft.jsch.HostKeyRepository;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

public class JnettopSshConnection implements JnettopConnection {

	private JnettopSshConnectionFactory	factory;
	private JnettopSshConnectionData	connectionData;

	private Shell parentShell;
	private ChannelExec execChannel;
	private Session sshSession;
	private JnettopSession jnSession;
	
	public JnettopSshConnection(JnettopSshConnectionFactory factory, Shell shell) {
		this.parentShell = shell;
		this.factory = factory;
	}

	public void setConnectionData(JnettopSshConnectionData data) {
		this.connectionData = data;
	}
	
	public JnettopConnectionData getConnectionData() {
		return this.connectionData;
	}

	/* (non-Javadoc)
	 * @see info.kubs.jnettop.gui.IJnettopSessionFactory#connect()
	 */
	public JnettopSession connect() throws Exception {
		JSch jsch = new JSch();
		if (connectionData.getIdentity() != null && connectionData.getIdentity().length() > 0) {
			jsch.addIdentity(connectionData.getIdentity());
		}
		jsch.setHostKeyRepository(new SshHostKeyRepository());
		sshSession = jsch.getSession(connectionData.getUserName(), connectionData.getHostName(), connectionData.getPort());
		sshSession.setUserInfo(new SshUserInfo());
		sshSession.connect();
		
		execChannel = (ChannelExec) sshSession.openChannel("exec");
		execChannel.setCommand(connectionData.getCommand());
		execChannel.setXForwarding(false);
		execChannel.setInputStream(null);
		execChannel.setOutputStream(null);
		execChannel.setErrStream(null);
		
		jnSession = new JnettopSession(execChannel.getInputStream(), execChannel.getOutputStream());
		jnSession.setSessionSite(new SessionSite());

		execChannel.connect();
		
		jnSession.initialize();
		
		return jnSession;
	}
	
	/* (non-Javadoc)
	 * @see info.kubs.jnettop.gui.IJnettopSessionFactory#close()
	 */
	public void close() {
		//jnSession.close();
		if (execChannel != null)
			execChannel.disconnect();
		if (sshSession != null)
			sshSession.disconnect();
	}
	
	public JnettopSession getSession() {
		return jnSession;
	}
	
	public JnettopConnectionFactory getFactory() {
		return factory;
	}
	
	private class SshUserInfo implements UserInfo {
		public String getPassphrase() {
			return PasswordMessageDialog.openPasswordQuery(parentShell, "SSH Key Passphrase", "Passphrase:");
		}

		public String getPassword() {
			return PasswordMessageDialog.openPasswordQuery(parentShell, "SSH Host Password", "Password:");
		}

		public boolean promptPassword(String arg0) {
			return true;
		}

		public boolean promptPassphrase(String arg0) {
			return true;
		}

		public boolean promptYesNo(String arg0) {
			return MessageDialog.openQuestion(parentShell, "SSH Question", arg0);
		}

		public void showMessage(String arg0) {
			MessageDialog.openInformation(parentShell, "SSH Information", arg0);
		}
	}
	
	private class SshHostKeyRepository implements HostKeyRepository {
		
		private HostKey readHostKey(String host, String type) {
			String keyData = JnettopGui.getInstance().getPreference("hostKey." + host + "." + type, null);
			if (keyData == null)
				return null;
			
			try {
				byte[] hostKeyData = Util.fromBase64(keyData);
				HostKey hk = new HostKey(host, hostKeyData);
				if (!hk.getType().equals(type))
					return null;
				return hk;
			} catch (JSchException e) {
				return null;
			}
		}
		
		private void writeHostKey(String host, byte[] key) {
			HostKey hk;
			try {
				hk = new HostKey(host, key);
			} catch (JSchException e) {
				return;
			}
			JnettopGui.getInstance().setPreference("hostKey." + host + "." + hk.getType(), Util.toBase64(key, 0, key.length));
		}
		
		private void removeHostKey(String host, String type) {
			JnettopGui.getInstance().removePreference("hostKey." + host + "." + type);
		}
		
		public int check(String host, byte[] key) {
			try {
				HostKey currentHostKey = new HostKey(host, key);
				HostKey savedHostKey = readHostKey(host, currentHostKey.getType());
				if (savedHostKey == null)
					return HostKeyRepository.NOT_INCLUDED;
				return currentHostKey.getKey().equals(savedHostKey.getKey()) ?
						HostKeyRepository.OK :
						HostKeyRepository.CHANGED;
			} catch (JSchException e) {
				return HostKeyRepository.NOT_INCLUDED;
			}
		}

		public void add(String host, byte[] key, UserInfo userinfo) {
			writeHostKey(host, key);
		}

		public void remove(String host, String type) {
			removeHostKey(host, type);
		}

		public void remove(String host, String type, byte[] key) {
			removeHostKey(host, type);
		}

		public String getKnownHostsRepositoryID() {
			return "JnettopGui";
		}

		public HostKey[] getHostKey() {
			return null;
		}

		public HostKey[] getHostKey(String host, String type) {
			HostKey hk = readHostKey(host, type);
			if (hk == null)
				return null;
			
			return new HostKey[] { hk };
		}
	}
	
	private class SessionSite implements JnettopSessionSite {
		public void showMessage(final String message) {
			parentShell.getDisplay().syncExec(new Runnable () {
				public void run() {
					MessageDialog.openInformation(parentShell, "Jnettop Session Information", message);
				}
			});		
		}
	}
}
