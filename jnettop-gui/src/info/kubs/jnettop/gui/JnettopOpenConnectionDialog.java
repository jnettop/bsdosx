/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/JnettopOpenConnectionDialog.java,v 1.1 2006-04-30 12:52:48 merunka Exp $
 *
 */

package info.kubs.jnettop.gui;


import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

public class JnettopOpenConnectionDialog extends Dialog {

	private List	connectionsList;
	private Button	deleteConnectionButton;
	private Button	newConnectionButton;
	
	private ArrayList<JnettopConnectionData>	connectionData;
	
	private JnettopConnectionData				createdConnectionData;

	public JnettopOpenConnectionDialog(Shell parentShell) {
		super(parentShell);
		connectionData = JnettopGui.getInstance().getConnectionData();
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Connect", true);
		createButton(parent, IDialogConstants.CANCEL_ID, "Cancel", false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		GridData gd;
		
		Composite mainComposite = new Composite(parent, 0);
		GridLayout gridLayout = new GridLayout(2, false);
		mainComposite.setLayout(gridLayout);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		connectionsList = new List(mainComposite, SWT.BORDER);
		gd = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd.minimumHeight = 150;
		gd.minimumWidth = 400;
		connectionsList.setLayoutData(gd);
		connectionsList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				okPressed();
			}
		});
		
		Composite buttonsComposite = new Composite(mainComposite, 0);
		gridLayout = new GridLayout();
		buttonsComposite.setLayout(gridLayout);
		buttonsComposite.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, true));
		
		newConnectionButton = new Button(buttonsComposite, 0);
		newConnectionButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				newConnectionButtonPressed();
			}
		});
		newConnectionButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));
		newConnectionButton.setText("New Connection...");
		
		deleteConnectionButton = new Button(buttonsComposite, 0);
		deleteConnectionButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteConnectionButtonPressed();
			}
		});
		deleteConnectionButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));
		deleteConnectionButton.setText("Delete");
		
		fillConnectionsList();
		mainComposite.layout();
		
		return mainComposite;
	}

	private void fillConnectionsList() {
		connectionsList.removeAll();
		for (JnettopConnectionData data : connectionData) {
			connectionsList.add(data.getDisplayName());
		}
	}
	
	private void newConnectionButtonPressed() {
		close();
		JnettopGui.getInstance().invokeNewConnectionDialog();
	}
	
	private void deleteConnectionButtonPressed() {
		int idx = connectionsList.getSelectionIndex();
		if (idx == -1)
			return;
		
		connectionData.remove(idx);
		fillConnectionsList();
	}

	@Override
	protected void cancelPressed() {
		setReturnCode(SWT.CANCEL);
		close();
	}

	@Override
	protected void okPressed() {
		int idx = connectionsList.getSelectionIndex();
		if (idx == -1)
			return;
		
		createdConnectionData = (JnettopConnectionData) connectionData.get(idx).clone();
		setReturnCode(SWT.OK);
		close();
	}
	
	public JnettopConnectionData getCreatedConnectionData() {
		return createdConnectionData;
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Connections");
	}
}
