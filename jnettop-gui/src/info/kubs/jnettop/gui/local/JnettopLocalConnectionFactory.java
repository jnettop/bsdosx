/*
 *    jnettop-gui, GUI for jnettop, network online traffic visualiser
 *    Copyright (C) 2002-2006 Jakub Skopal
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    $Header: /home/jakubs/DEV/jnettop-conversion/jnettop-gui/src/info/kubs/jnettop/gui/local/JnettopLocalConnectionFactory.java,v 1.1 2006-04-30 12:52:25 merunka Exp $
 *
 */

package info.kubs.jnettop.gui.local;

import info.kubs.jnettop.gui.JnettopConnection;
import info.kubs.jnettop.gui.JnettopConnectionData;
import info.kubs.jnettop.gui.JnettopConnectionFactory;
import info.kubs.jnettop.gui.JnettopEditConnectionControl;

import java.util.prefs.Preferences;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

public class JnettopLocalConnectionFactory extends JnettopConnectionFactory {

	public JnettopLocalConnectionFactory() {
		super("Local exec");
	}
	
	@Override
	public JnettopConnectionData createConnectionData(String prefix, Preferences prefs) {
		return new JnettopLocalConnectionData(this, prefix, prefs);
	}

	@Override
	public JnettopEditConnectionControl createEditConnectionControl(Composite parent) {
		JnettopEditLocalConnectionControl cntrl = new JnettopEditLocalConnectionControl(parent, 0, this);
		return cntrl;
	}

	@Override
	public JnettopConnection createConnection(Shell parentShell, JnettopConnectionData data) {
		JnettopLocalConnectionData localData = (JnettopLocalConnectionData) data;
		
		JnettopLocalConnection localConnection = new JnettopLocalConnection(this, parentShell);
		localConnection.setConnectionData(localData);
		return localConnection;
	}	
}
